import { isEmpty } from 'lodash'

export const setHttpToken = (token) => {
	if(isEmpty(token))
		token = null;
		
	window.axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}
