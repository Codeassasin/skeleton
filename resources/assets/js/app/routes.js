import auth from './auth/routes'
import home from './home/routes'
import errors from './errors/routes'
import dashboard from './dashboard/routes'

export default [
	...auth,
	...home,
	...dashboard,
	...errors // Must always be at the End
]
