import { Dashboard } from '../components'

export default [
	{
		path: '/dashboard',
		component: Dashboard,
		name: 'dashboard',
		meta: {
			title: 'Dashboard',
			guest: false,
			needsAuth: true
		}
	}
]
