import { isEmpty } from 'lodash'
import localforage from 'localforage'
import { setHttpToken } from '../../../helpers'


/**
 * Register a user in the System
 */
export const register = ({ dispatch }, { payload, context }) => {
	return axios.post('/api/register', payload).then((response) => {
		dispatch('setToken', response.data.meta.token).then(() => {
			dispatch('fetchUser')
		})
	}).catch((errors) => {
		context.errors = errors.response.data.errors
	})
}


/**
 * Login a user in the System
 */
export const login = ({ dispatch }, { payload, context }) => {
	return axios.post('/api/login', payload).then((response) => {
		dispatch('setToken', response.data.meta.token).then(() => {
			dispatch('fetchUser')
		})
	}).catch((errors) => {
		context.errors = errors.response.data.errors
	})
}


/**
 * Logout the User from the System
 */
export const logout = ({ dispatch }) => {
	return axios.post('/api/logout').then((response) => {
		dispatch('clearAuth')
	})
}


/**
 * Set the Authenticated User Token
 */
export const setToken = ({ commit, dispatch }, token) => {

	if( isEmpty(token)) {
		return dispatch('checkTokenExists').then((token) => {
			setHttpToken(token)
		})
	}

	commit('setToken', token)
	setHttpToken(token)
}


/**
 * Fetch the Authenticated User's data
 */
export const fetchUser = ({ commit }) => {
	return axios.get('/api/me').then((response) => {
		commit('setAuthenticated', true)
		commit('setUserData', response.data.data)
	})
}


/**
 * Check if Token exists in Local Storage
 */
export const checkTokenExists = ({ commit, dispatch }, token) => {
	return localforage.getItem('authtoken').then((token) => {

		if( isEmpty(token))
			return Promise.reject("NO_STORAGE_TOKEN")

		return Promise.resolve(token)
	})
}


/**
 * Clear Application Authentication State
 */
export const clearAuth = ({ commit }, token) => {
	commit('setAuthenticated', false)
	commit('setUserData', null)
	commit('setToken', null)
	setHttpToken(null)
}
