/**
 * Retrieve User from State
 */
export const user = (state) => {
	return state.user
}
