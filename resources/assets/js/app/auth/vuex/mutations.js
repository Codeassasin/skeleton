import localforage from 'localforage'
import { isEmpty } from 'lodash'


/**
 * Set the JWT token
 */
export const setToken = (state, token) => {

	if(isEmpty(token))
		return localforage.removeItem('authtoken')

	localforage.setItem('authtoken', token)
}


/**
 * Set State Authenticated
 */
export const setAuthenticated = (state, trueOrFalse) => {
	return state.user.authenticated = trueOrFalse
}


/**
 * Set State User Data
 */
export const setUserData = (state, data) => {
	return state.user.data = data
}
