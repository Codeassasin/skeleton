import { Login, Register } from '../components'

export default [
	{
		path: '/login',
		component: Login,
		name: 'login',
		meta: {
			title: 'Login',
			guest: true,
			needsAuth: false
		}
	},
	{
		path: '/register',
		component: Register,
		name: 'register',
		meta: {
            title: 'Register',
			guest: true,
			needsAuth: false
		}
	}
]
