import Vue from 'vue';
import store from './vuex'
import router from './router'
import localforage from 'localforage'

/**
 * Require Bootstrap JS
 */
require('./bootstrap');


/**
 * Configure localforage
 */
localforage.config({
    driver: localforage.LOCALSTORAGE,
    storeName: 'skeleton'
})


/**
 * Register Top-Level Application Components
 */
Vue.component('app', require('./components/App.vue'));
Vue.component('navigation', require('./components/Navigation.vue'));


/**
 * Handle Token Persistence during page reloads
 */
store.dispatch('auth/setToken').then(() => {
    store.dispatch('auth/fetchUser').catch(() => {
        store.dispatch('auth/clearAuth')
        router.replace({ name: 'login'})
    })
}).catch(() => {
    store.dispatch('auth/clearAuth')
})


/**
 * Bootstrap Application
 */
const app = new Vue({
    el: '#app',
    store,
    router
});
