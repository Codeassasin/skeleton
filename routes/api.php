<?php

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\loginController@login');
Route::post('logout', 'Auth\loginController@logout');

Route::group(['middleware' => 'jwt.auth'], function() {
	Route::get('me', 'Auth\LoginController@user');
});
