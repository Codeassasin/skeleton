<?php

namespace Skeleton\Jobs;

use Skeleton\Entities\Users\User;
use Illuminate\Foundation\Bus\Dispatchable;

class RegisterUser
{
    use Dispatchable;

    protected $first_name, $last_name, $email, $password;

    /**
     * Create a new job instance.
     *
     * @param String $first_name
     * @param String $last_name
     * @param String $email
     * @param String $password
     */
    public function __construct($first_name, $last_name, $email, $password)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->password = bcrypt($password);
    }

    /**
     * Register a new user in the database.
     *
     * @param User $user
     * @return void
     */
    public function handle(User $user)
    {
        $user = $user->register($this->first_name, $this->last_name, $this->email, $this->password);

        $user->save();
    }
}
