<?php

namespace Skeleton\Http\Controllers\Auth;

use Tymon\JWTAuth\JWTAuth;
use Skeleton\Http\Controllers\Controller;
use Skeleton\Http\Requests\Auth\LoginRequest;
use Tymon\JWTAuth\Exceptions\{JWTException};

class LoginController extends Controller
{
    /**
     * @var JWTAuth
     */
	protected $auth;

    /**
     * Inject JWTAuth
     *
     * @param JWTAuth $auth
     */
    public function __construct(JWTAuth $auth)
    {
		$this->auth = $auth;
    }

    /**
     * Process Login Request
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
		try {
			$token = $this->auth->attempt($request->only('email', 'password'));

			if(! $token) {
				return response()->json([
					'errors' => [
						'root' => 'Could not sign you in with the provided details'
					]
				], 401);
			}
		} catch (JWTException $e) {
			return response()->json([
				'errors' => [
					'root' => 'Failed'
				]
			], $e->getStatusCode());
		}

		return response()->json([
			'data' => auth()->user(),
			'meta' => [
				'token' => $token
			]
		], 200);
    }

    /**
     * Logged in User
     *
     * @return \Illuminate\Http\JsonResponse
     */
	public function user()
	{
		return response()->json([
			'data' => request()->user()
		]);
	}

    /**
     * Logout a User
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
	public function logout()
	{
		$this->auth->invalidate($this->auth->getToken());

		return response(null, 200);
	}
}
