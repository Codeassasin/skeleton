<?php

namespace Skeleton\Http\Controllers\Auth;

use Tymon\JWTAuth\JWTAuth;
use Skeleton\Jobs\RegisterUser;
use Skeleton\Http\Controllers\Controller;
use Skeleton\Http\Requests\Auth\RegisterRequest;

class RegisterController extends Controller
{
    /**
     * @var JWTAuth
     */
	protected $auth;

    /**
     * Inject JWTAuth
     *
     * @param JWTAuth $auth
     */
    public function __construct(JWTAuth $auth)
    {
		$this->auth = $auth;
    }

    /**
     * Register a new user on the system
     *
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
	public function register(RegisterRequest $request)
	{
		$this->dispatch(new RegisterUser(
			$request->first_name,
			$request->last_name,
			$request->email,
			$request->password
		));

		$token = $this->auth->attempt($request->only('email', 'password'));

		return response()->json([
			'data' => auth()->user(),
			'meta' => [
				'token' => $token
			]
		], 200);
	}
}
