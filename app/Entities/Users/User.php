<?php

namespace Skeleton\Entities\Users;

use Skeleton\Traits\Common\Uuids;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Handle the Registration of a new User
     *
     * @param  String $first_name
     * @param  String $last_name
     * @param  String $email
     * @param  String $password
     * @return $this
     */
    public function register($first_name, $last_name, $email, $password)
    {
        return new static(compact('first_name', 'last_name', 'email', 'password'));
    }
}
